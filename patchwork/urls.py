# Patchwork - automated patch tracking system
# Copyright (C) 2008 Jeremy Kerr <jk@ozlabs.org>
#
# This file is part of the Patchwork package.
#
# Patchwork is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Patchwork is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Patchwork; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from django.conf import settings
from django.conf.urls import include
from django.urls import path, re_path
from django.contrib import admin
from django.contrib.auth import views as auth_views
from rest_framework_nested import routers
from patchwork.views.series import SeriesListView, SeriesView
from django.views.generic import TemplateView
import patchwork.views.api as api
import patchwork.views
import patchwork.views.bundle
import patchwork.views.project
import patchwork.views.patch
import patchwork.views.user
import patchwork.views.mail
import patchwork.views.base
import patchwork.views.xmlrpc

# API

# /projects/$project/
project_router = routers.SimpleRouter()
project_router.register('projects', api.ProjectViewSet)
# /projects/$project/patches/
patches_list_router = routers.NestedSimpleRouter(project_router, 'projects',
                                                 lookup='project')
patches_list_router.register(r'patches', api.PatchListViewSet)
# /projects/$project/series/
series_list_router = routers.NestedSimpleRouter(project_router, 'projects',
                                                lookup='project')
series_list_router.register(r'series', api.SeriesListViewSet)
# /projects/$project/events/
event_router = routers.NestedSimpleRouter(project_router, 'projects',
                                          lookup='project')
event_router.register(r'events', api.EventLogViewSet)
# /series/$id/
series_router = routers.SimpleRouter()
series_router.register(r'series', api.SeriesViewSet)
# /series/$id/revisions/$rev
revisions_router = routers.NestedSimpleRouter(series_router, 'series',
                                              lookup='series')
revisions_router.register(r'revisions', api.RevisionViewSet)
# /series/$id/revisions/$rev/test-results/
revision_results_router = routers.NestedSimpleRouter(revisions_router,
                                                     'revisions',
                                                     lookup='version')
revision_results_router.register(r'test-results', api.RevisionResultViewSet,
                                 basename='revision-results')
# /patches/$id/
patches_router = routers.SimpleRouter()
patches_router.register(r'patches', api.PatchViewSet)
# /patches/$id/test-results/
patch_results_router = routers.NestedSimpleRouter(patches_router, 'patches',
                                                  lookup='patch')
patch_results_router.register(r'test-results', api.PatchResultViewSet,
                              basename='patch-results')
# /test-results/
test_results_router = routers.SimpleRouter()
test_results_router.register(r'test-results', api.TestResultsViewSet)


admin.autodiscover()

urlpatterns = [
    path('robots.txt', TemplateView.as_view(template_name="robots.txt",
                                            content_type="text/plain")),

    path('admin/', admin.site.urls),

    # API
    re_path(r'^api/1.0/$', api.API.as_view(), name='api-root'),
    re_path(r'^api/1.0/now$', api.now),
    re_path(r'^api/1.0/', include(project_router.urls)),
    re_path(r'^api/1.0/', include(patches_list_router.urls)),
    re_path(r'^api/1.0/', include(series_list_router.urls)),
    re_path(r'^api/1.0/', include(series_router.urls)),
    re_path(r'^api/1.0/', include(revisions_router.urls)),
    re_path(r'^api/1.0/', include(revision_results_router.urls)),
    re_path(r'^api/1.0/', include(patches_router.urls)),
    re_path(r'^api/1.0/', include(patch_results_router.urls)),
    re_path(r'^api/1.0/', include(event_router.urls)),
    re_path(r'^api/1.0/', include(test_results_router.urls)),
    re_path(r'^api/1.0/msgids/(?P<msgid>[^/]+)/$',
            api.MsgidResultsView.as_view(), name='msgid_to_series'),

    # project views:
    re_path(r'^$', patchwork.views.base.projects,
        name='root'),
    re_path(r'^project/(?P<project_id>[^/]+)/list/$',
            patchwork.views.patch.list, name='patch_list'),
    re_path(r'^project/(?P<project_id>[^/]+)/patches/$',
        patchwork.views.patch.list,
        name='patches_list'),
    re_path(r'^project/(?P<project_id>[^/]+)/$',
            patchwork.views.project.project, name='project'),

    # series views
    re_path(r'^project/(?P<project>[^/]+)/series/$', SeriesListView.as_view(),
        name='series_list'),
    re_path(r'^series/(?P<series>\d+)/$', SeriesView.as_view(),
        name='series'),

    re_path(r'^series/msgid/(?P<msgid>[^/]+)/$', patchwork.views.series.msgid,
        name='series_msgid'),

    # patch views
    re_path(r'^patch/(?P<patch_id>\d+)/$', patchwork.views.patch.patch,
        name='patch'),
    re_path(r'^patch/(?P<patch_id>\d+)/raw/$', patchwork.views.patch.content,
        name='patch_content'),
    re_path(r'^patch/(?P<patch_id>\d+)/mbox/$', patchwork.views.patch.mbox,
        name='patch_mbox'),
    re_path(r'^patch/msgid/(?P<msgid>[^/]+)/$', patchwork.views.patch.msgid,
        name='patch_msgid'),

    # project bundles
    re_path(r'^project/(?P<project_id>[^/]+)/bundles/$',
        patchwork.views.bundle.bundles,
        name='bundle_list'),

    # logged-in user stuff
    re_path(r'^user/$', patchwork.views.user.profile,
        name='user'),
    re_path(r'^user/todo/$', patchwork.views.user.todo_lists,
        name='todo_list'),
    re_path(r'^user/todo/(?P<project_id>[^/]+)/$',
            patchwork.views.user.todo_list, name='todo_list'),

    re_path(r'^user/bundles/$', patchwork.views.bundle.bundles,
        name='bundle_list'),

    re_path(r'^user/link/$', patchwork.views.user.link,
        name='user_link'),
    re_path(r'^user/unlink/(?P<person_id>[^/]+)/$',
            patchwork.views.user.unlink, name='user_unlink'),

    # password change
    path('user/password-change/',
        auth_views.PasswordChangeView.as_view(),
        name='password_change'),
    path('user/password-change/done/',
        auth_views.PasswordChangeDoneView.as_view(),
        name='password_change_done'),
    path('user/password-reset/',
        auth_views.PasswordResetView.as_view(),
        name='password_reset'),
    path('user/password-reset/mail-sent/',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done'),
    re_path(r'^user/password-reset/(?P<uidb64>[0-9A-Za-z_\-]+)'
        r'/(?P<token>[0-9A-Za-z]+-[0-9A-Za-z]+)/$',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'),
    path('user/password-reset/complete/',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete'),

    # login/logout
    path('user/login/', auth_views.LoginView.as_view(
            template_name='patchwork/login.html'
        ),
        name='auth_login'),
    path('user/logout/', auth_views.LogoutView.as_view(next_page='/'),
        name='auth_logout'),

    # registration
    re_path(r'^register/', patchwork.views.user.register,
        name='register'),

    # public view for bundles
    re_path(r'^bundle/(?P<username>[^/]*)/(?P<bundlename>[^/]*)/$',
     patchwork.views.bundle.bundle, name='bundle'),
    re_path(r'^bundle/(?P<username>[^/]*)/(?P<bundlename>[^/]*)/mbox/$',
     patchwork.views.bundle.mbox, name='bundle_mbox'),

    re_path(r'^confirm/(?P<key>[0-9a-f]+)/$', patchwork.views.base.confirm,
        name='confirm'),

    # submitter autocomplete
    re_path(r'^submitter/$', patchwork.views.submitter_complete),
    # user autocomplete
    re_path(r'^complete_user/$', patchwork.views.user_complete),

    # email setup
    re_path(r'^mail/$', patchwork.views.mail.settings,
        name='mail_settings'),
    re_path(r'^mail/optout/$', patchwork.views.mail.optout,
        name='mail_optout'),
    re_path(r'^mail/optin/$', patchwork.views.mail.optin,
        name='mail_optin'),

    # help!
    re_path(r'^help/(?P<path>.*)$', patchwork.views.base.help,
        name='help'),
]

if 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

if settings.ENABLE_XMLRPC:
    urlpatterns += [
                   re_path(r'xmlrpc/$', patchwork.views.xmlrpc.xmlrpc,
                       name='xmlrpc'),
                   re_path(r'^pwclient/$', patchwork.views.pwclient,
                       name='pwclient'),
                   re_path(r'^project/(?P<project_id>[^/]+)/pwclientrc/$',
                       patchwork.views.pwclientrc,
                       name='pwclientrc'),
                   ]

# redirect from old urls
if settings.COMPAT_REDIR:
    urlpatterns += [
                   re_path(r'^user/bundle/(?P<bundle_id>[^/]+)/$',
                    patchwork.views.bundle.bundle_redir),
                   re_path(r'^user/bundle/(?P<bundle_id>[^/]+)/mbox/$',
                   patchwork.views.bundle.mbox_redir),
                   ]
