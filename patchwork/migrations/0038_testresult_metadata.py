# Generated by Django 3.2.5 on 2021-07-24 21:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patchwork', '0037_auto_20200715_2327'),
    ]

    operations = [
        migrations.AddField(
            model_name='testresult',
            name='metadata',
            field=models.JSONField(blank=True, null=True),
        ),
    ]
